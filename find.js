const cumstomFind = function find(elements, cb) {
    if (!Array.isArray(elements)) {
        throw Error('parameter one has to be an array');
    }
    if (typeof cb != 'function' || !cb) {
        throw Error('callback paramenter is not a function');
    }
    if (!elements) {
        throw Error('Array called on null or undefined');
    }

    for (let index = 0; index < elements.length; index++) {
        const test = cb(elements[index], index);

        if (test) {
            return elements[index];
        }
    }
}

module.exports = cumstomFind;





