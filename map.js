const mapping = function map(elements, iteratee) {
    if (!Array.isArray(elements)) {
        throw Error('parameter one has to be an array');
    }
    if (typeof iteratee != 'function' || !iteratee) {
        throw Error('callback paramenter is not a function');
    }
    if (!elements) {
        throw Error('Array called on null or undefined');
    }

    const newItems = [];

    for (let index = 0; index < elements.length; index++) {

        const ele = iteratee(elements[index], index);
        newItems.push(ele);
    }

    return newItems;
}

module.exports = mapping;





