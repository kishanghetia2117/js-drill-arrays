const find = require('../find.js');

const items = [1, 2, 3, 4, 5, 5];

// testcase 1
const cb = ele => ele > 3;
let testResultOne = find(items, cb);
const prototypeResultTest1 = items.find(cb);

checktestCase(testResultOne, prototypeResultTest1);

// testcase 1
function isPrime(element) {
    let start = 2;
    while (start <= Math.sqrt(element)) {
        if (element % start++ < 1) {
            return false;
        }
    }
    return element > 1;
}
let testResultTwo = find(items, isPrime);
const prototypeResultTest2 = items.find(isPrime);

checktestCase(testResultTwo, prototypeResultTest2);

// checking testscases 1,2 results 
function checktestCase(result, prototypeResultTest) {

    function verifyTestCase1(result, prototypeResultTest) {
        if (result !== prototypeResultTest) {
            return false;
        } else {
            return true;
        }
    }

    if (result === null || result === undefined || prototypeResultTest === null || prototypeResultTest === undefined) {
        console.log("result or prototypeResultTest null/undefined")
    } else {
        const verifiedOUtput = verifyTestCase1(result, prototypeResultTest);

        if (verifiedOUtput) {
            console.log("Test case result matchced");
            console.log(`result = ${result}`);
        } else {
            console.log("Test case failed: Result did not match");
        }
    }
}