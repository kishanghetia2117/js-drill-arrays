const reduce = require('../reduce.js');


const items = [1, 2, 3, 4, 5, 5];
const cb = (reducerValue, elem) => reducerValue + elem;

// testecase 1
let testResultOne = reduce(items, cb, 0);
const prototypeResultTest1 = items.reduce(cb);

checktestCase(testResultOne, prototypeResultTest1);

// testecase 2
let startingValue = 10;

let testResultTwo = reduce(items, cb, startingValue);
const prototypeResultTest2 = items.reduce(cb, startingValue);

checktestCase(testResultTwo, prototypeResultTest2);

// checking testscases 1,2 results 
function checktestCase(result, prototypeResultTest) {

    function verifyTestCase1(result, prototypeResultTest) {
        if (result !== prototypeResultTest) {
            return false;
        } else {
            return true;
        }
    }

    if (result === null || result === undefined || prototypeResultTest === null || prototypeResultTest === undefined) {
        console.log("result or prototypeResultTest null/undefined")
    } else {
        const verifiedOUtput = verifyTestCase1(result, prototypeResultTest);

        if (verifiedOUtput) {
            console.log("Test case result matchced");
            console.log(`result = ${result}`);
        } else {
            console.log("Test case failed: Result did not match");
        }
    }
}
