const flatten = require('../flatten.js');

const nestedArray = [1, [2], [[3]], [[[4]]]];

//testcase 1
let result = flatten(nestedArray);
let expectedOutput = nestedArray.flat(Infinity);

checktestCase(result, expectedOutput)

// result check 
function checktestCase(result, prototypeResultTest) {

    function verifyTestCase1(result, prototypeResultTest) {
        for (index = 0; index < result.length; index++) {
            if (result[index] !== prototypeResultTest[index]) {
                return false;
            }
        }
        return true;
    }

    if (result === null || result === undefined || prototypeResultTest === null || prototypeResultTest === undefined) {
        console.log("result or prototypeResultTest null/undefined")
    } else {
        const verifiedOUtput = verifyTestCase1(result, prototypeResultTest);

        if (verifiedOUtput) {
            console.log("Test case result matchced");
            console.log(`result = [${result}]`);
        } else {
            console.log("Test case failed: Result did not match");
        }
    }
}