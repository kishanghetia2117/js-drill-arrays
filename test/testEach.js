const each = require('../each.js');

const items = [1, 2, 3, 4, 5, 5];

const cb = (elem, index) => console.log(elem, index);

let testResultOne = each(items, cb);
const prototypeResultTest1 = items.forEach(cb);

