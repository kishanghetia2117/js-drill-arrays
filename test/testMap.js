const map = require('../map.js');

const items = [1, 2, 3, 4, 5, 5];

// testcase 1
let testResultOne = map(items, ele => ele);
const prototypeResultTest1 = items.map(ele => ele);

checktestCase(testResultOne, prototypeResultTest1)

// testcase 2
const cb = ele => Math.sqrt(ele);
let testResultTwo = map(items, cb);
const prototypeResultTest2 = items.map(cb);

checktestCase(testResultTwo, prototypeResultTest2)

// checking testscases 1,2 results 
function checktestCase(result, prototypeResultTest) {

    function verifyTestCase1(result, prototypeResultTest) {
        for (index = 0; index < result.length; index++) {
            if (result[index] !== prototypeResultTest[index]) {
                return false;
            }
        }
        return true;
    }

    if (result === null || result === undefined || prototypeResultTest === null || prototypeResultTest === undefined) {
        console.log("result or prototypeResultTest null/undefined")
    } else {
        const verifiedOUtput = verifyTestCase1(result, prototypeResultTest);

        if (verifiedOUtput) {
            console.log("Test case result matchced");
            console.log(`result = [${result}]`);
        } else {
            console.log("Test case failed: Result did not match");
        }
    }
}

