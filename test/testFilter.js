const filter = require('../filter.js');

const items = [1, 2, 3, 4, 5, 5];

// testcase 1
const cb = ele => ele > 2 && ele < 5;
let testResultOne = filter(items, cb);
const prototypeResultTest1 = items.filter(cb);

checktestCase(testResultOne, prototypeResultTest1);

// testcase 2
function isPrime(num) {
    for (let index = 2; num > index; index++) {
        if (num % index == 0) {
            return false;
        }
    }
    return num > 1;
}
let testResultTwo = filter(items, isPrime);
const prototypeResultTest2 = items.filter(isPrime);

checktestCase(testResultTwo, prototypeResultTest2);

// result check 
function checktestCase(result, prototypeResultTest) {

    function verifyTestCase1(result, prototypeResultTest) {
        for (index = 0; index < result.length; index++) {
            if (result[index] !== prototypeResultTest[index]) {
                return false;
            }
        }
        return true;
    }

    if (result === null || result === undefined || prototypeResultTest === null || prototypeResultTest === undefined) {
        console.log("result or prototypeResultTest null/undefined")
    } else {
        const verifiedOUtput = verifyTestCase1(result, prototypeResultTest);

        if (verifiedOUtput) {
            console.log("Test case result matchced");
            console.log(`result = [${result}]`);
        } else {
            console.log("Test case failed: Result did not match");
        }
    }
}