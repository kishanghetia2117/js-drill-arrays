const reducer = function reduce(elements, cb, startingValue) {
    if (!Array.isArray(elements)) {
        throw Error('parameter one has to be an array');
    }
    if (typeof cb != 'function' || !cb) {
        throw Error('callback paramenter is not a function');
    }
    if (!elements) {
        throw Error('Array called on null or undefined');
    }

    let reducerValue = startingValue || elements[0];

    for (let index = 0; index < elements.length; index++) {
        if (index === 0 && !startingValue) {
            continue;
        }

        reducerValue = cb(reducerValue, elements[index], index)
    }
    return reducerValue;
}

module.exports = reducer;





