const getFlattenArr = function flatten(nestedArray) {
    let resultArr = [];

    for (let ele = 0; ele < nestedArray.length; ele++) {
        if (Array.isArray(nestedArray[ele])) {
            const subArr = flatten(nestedArray[ele]);
            for (let ele = 0; ele < subArr.length; ele++) {
                resultArr.push(subArr[ele]);
            }
        }
        else {
            resultArr.push(nestedArray[ele]);
        }
    }
    return resultArr;
}

module.exports = getFlattenArr;





